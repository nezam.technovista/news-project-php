<?php
include "header.php";
include "check-user.php";
?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1 class="admin-heading">All Users</h1>
            </div>
            <div class="col-md-2">
                <a class="add-new" href="add-user.php">add user</a>
            </div>
            <div class="col-md-12">
                <?php
                include "config.php";
                $limit = 3;
                if (isset($_GET["pageId"])) {
                    $page = $_GET["pageId"];
                } else {
                    $page = 1;
                }
                $offset = ($page - 1) * $limit;

                $sql1 = "SELECT * FROM user ORDER BY user_id LIMIT {$offset}, {$limit}";
                $query1 = mysqli_query($conn, $sql1) or die("Query Faild");
                if (mysqli_num_rows($query1) > 0) {

                ?>
                    <table class="content-table">
                        <thead>
                            <th>S.No.</th>
                            <th>Full Name</th>
                            <th>User Name</th>
                            <th>Role</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            <?php
                            while ($row1 = mysqli_fetch_assoc($query1)) {
                            ?>
                                <tr>
                                    <td class='id'><?php echo $row1["user_id"]; ?></td>
                                    <td><?php echo $row1["first_name"] . " " . $row1["last_name"]; ?></td>
                                    <td><?php echo $row1["user_name"]; ?></td>
                                    <td><?php
                                        if ($row1["role"] == 1) {
                                            echo "Admin";
                                        } else {
                                            echo "Visitor";
                                        }
                                        ?></td>
                                    <td class='edit'><a href='update-user.php?id=<?php echo $row1["user_id"] ?>'><i class='fa fa-edit'></i></a></td>
                                    <td class='delete'><a href='delete-user.php?id=<?php echo $row1["user_id"] ?>'><i class='fa fa-trash-o'></i></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php
                }
                $sql2 = "SELECT * FROM user";
                $query2 = mysqli_query($conn, $sql2) or die("Query faild");
                if (mysqli_num_rows($query2) > 0) {
                    $total_records = mysqli_num_rows($query2);
                    $total_pages = ceil($total_records / $limit);
                    echo "<ul class='pagination admin-pagination'>";
                    if ($page > 1) {
                        echo "<li><a href='users.php?pageId=" . ($page - 1) . "'>Prev</a></li>";
                    }
                    for ($i = 1; $i <= $total_pages; $i++) {
                        if ($i == $page) {
                            $active = "active";
                        } else {
                            $active = "";
                        }
                        echo "<li class='" . $active . "'><a href='users.php?pageId=$i'>" . "$i" . "</a></li>";
                    }
                    if ($total_pages > $page) {
                        echo "<li><a href='users.php?pageId=" . ($page + 1) . "'>Next</a></li>";
                    }
                    echo "</ul>";
                }
                ?>

                <!-- <li class="active"><a>1</a></li> -->

                <!-- <li><a>3</a></li> -->

            </div>
        </div>
    </div>
</div>
<?php include "header.php"; ?>