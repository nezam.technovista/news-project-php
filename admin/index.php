<?php
include "config.php";
session_start();
if (isset($_SESSION["user_name"])) {
    header("Location: {$host_name}/admin/post.php");
}
?>

<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADMIN | Login</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="font/font-awesome-4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <div id="wrapper-admin" class="body-content">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <img class="logo" src="images/news.jpg">
                    <h3 class="heading">Admin</h3>
                    <!-- Form Start -->
                    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="user_name" class="form-control" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="" required>
                        </div>
                        <input type="submit" name="login" class="btn btn-primary" value="login" />
                    </form>
                    <!-- /Form  End -->
                    <?php
                    if (isset($_POST['login'])) {
                        include "config.php";
                        $user_name = mysqli_real_escape_string($conn, $_POST["user_name"]);
                        $password = md5($_POST["password"]);

                        $sql1 = "SELECT user_id, user_name, role FROM user WHERE user_name ='{$user_name}' AND password ='{$password}' ";
                        $query1 = mysqli_query($conn, $sql1) or die("Query Faild");

                        if (mysqli_num_rows($query1)) {
                            while ($row1 = mysqli_fetch_assoc($query1)) {
                                session_start();
                                $_SESSION["user_id"] = $row1["user_id"];
                                $_SESSION["user_name"] = $row1["user_name"];
                                $_SESSION["role"] = $row1["role"];
                                header("Location: {$host_name}/admin/post.php");
                                // mysqli_close($conn);
                            }
                        } else {
                            echo "<div class = 'alart alart-danger'> user Name and Password did not match </div>";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>