<?php
include "header.php";
include "check-user.php";

if (isset($_POST['submit'])) {
    include "config.php";
    $user_id = mysqli_real_escape_string($conn, $_POST['user_id']);
    $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
    $last_name = mysqli_real_escape_string($conn, $_POST['last_name']);
    $user_name = mysqli_real_escape_string($conn, $_POST['user_name']);
    // $password = mysqli_real_escape_string($conn, md5($_POST['password']));
    $role = mysqli_real_escape_string($conn, $_POST["role"]);

    $sql1 = " UPDATE user SET first_name = '{$first_name}', last_name = '{$last_name}', user_name = '{$user_name}', role = '{$role}' WHERE user_id = {$user_id} ";
    $query1 = mysqli_query($conn, $sql1) or die("sorry Query faild");

    if ($query1) {
        header("Location: {$host_name}/admin/users.php");
        mysqli_close($conn);
    } else {
        echo "<p>can not update user.</p>";
    }
}
?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="admin-heading">Modify User Details</h1>
            </div>
            <div class="col-md-offset-4 col-md-4">
                <?php
                include "config.php";
                $user_id = $_GET["id"];
                $sql2 = "SELECT * FROM user WHERE user_id = {$user_id}";
                $query2 = mysqli_query($conn, $sql2) or die("query Faild");
                if (mysqli_num_rows($query2) > 0) {
                    while ($row1 = mysqli_fetch_assoc($query2)) {

                ?>
                        <!-- Form Start -->
                        <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="user_id" class="form-control" value="<?php echo $row1['user_id'] ?>" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control" value="<?php echo $row1['first_name'] ?>" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control" value="<?php echo $row1['last_name'] ?>" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>User Name</label>
                                <input type="text" name="user_name" class="form-control" value="<?php echo $row1['user_name'] ?>" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>User Role</label>
                                <select class="form-control" name="role" value="<?php echo $row['role']; ?>">

                                    <?php
                                    if ($row1["role"] == 1) {
                                        echo "<option value='0'>Visitor</option>
                                            <option value='1' selected >Admin</option>";
                                    } else {
                                        echo "<option value='0' selected>Visitor</option>
                                            <option value='1'  >Admin</option>";
                                    }

                                    ?>

                                </select>
                            </div>
                            <input type="submit" name="submit" class="btn btn-primary" value="Update" required />
                        </form>
                <?php
                    }
                }
                ?>
                <!-- /Form -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>