<?php

include "config.php";

$post_id = $_GET["id"];
$cat_id = $_GET["catId"];

$sqlDeleteImage = "SELECT * FROM post WHERE post_id = {$post_id}";
$deleteImageQuery = mysqli_query($conn, $sqlDeleteImage);
$row = mysqli_fetch_assoc($deleteImageQuery);
unlink("upload/" . $row["post_img"]);

$sql = "DELETE FROM post WHERE post_id = {$post_id};";
$sql .= "UPDATE category SET post = post - 1 WHERE category_id = {$cat_id}";

if (mysqli_multi_query($conn, $sql)) {
   header("Location: {$host_name}/admin/post.php");
} else {
   echo "sorry can not delete post";
}
