<?php
include "header.php";
include "check-user.php";

if (isset($_POST['save'])) {
    include "config.php";
    $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
    $last_name = mysqli_real_escape_string($conn, $_POST['last_name']);
    $user_name = mysqli_real_escape_string($conn, $_POST['user_name']);
    $password = mysqli_real_escape_string($conn, md5($_POST['password']));
    $role = mysqli_real_escape_string($conn, $_POST["role"]);

    $sql1 = " SELECT user_name FROM user WHERE user_name = '{$user_name}' ";
    $query1 = mysqli_query($conn, $sql1) or die("sorry Query faild");
    if (mysqli_num_rows($query1) > 0) {
        echo "<p>user Name already exist.</p>";
    } else {
        $sql2 = " INSERT INTO user(first_name, last_name, user_name, password, role) 
            VALUES('{$first_name}','{$last_name}','{$user_name}','{$password}', '{$role}' ) ";
        $query2 = mysqli_query($conn, $sql2);
        if (!$query2) {
            echo "can not add user";
            exit();
        } else {
            header("Location: {$host_name}/admin/users.php");
            mysqli_close($conn);
        }
    }
}
?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="admin-heading">Add User</h1>
            </div>
            <div class="col-md-offset-3 col-md-6">
                <!-- Form Start -->
                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control" placeholder="First Name" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" required>
                    </div>
                    <div class="form-group">
                        <label>User Name</label>
                        <input type="text" name="user_name" class="form-control" placeholder="Username" required>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label>User Role</label>
                        <select class="form-control" name="role">
                            <option value="0">Normal User</option>
                            <option value="1">Admin</option>
                        </select>
                    </div>
                    <input type="submit" name="save" class="btn btn-primary" value="Save" required />
                </form>
                <!-- Form End-->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>