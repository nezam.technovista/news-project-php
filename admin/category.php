<?php
include "header.php";
include "check-user.php";

?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1 class="admin-heading">All Categories</h1>
            </div>
            <div class="col-md-2">
                <a class="add-new" href="add-category.php">add category</a>
            </div>
            <div class="col-md-12">
                <?php
                include "config.php";
                $sql1 = "SELECT * FROM category";
                $query1 = mysqli_query($conn, $sql1) or die("Query Faild");
                if (mysqli_num_rows($query1) > 0) {

                ?>
                    <table class="content-table">
                        <thead>
                            <th>S.No.</th>
                            <th>Category Name</th>
                            <th>No. of Posts</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            <?php
                            while ($row1 = mysqli_fetch_assoc($query1)) {

                            ?>
                                <tr>
                                    <td class='id'><?php echo $row1["category_id"]; ?></td>
                                    <td><?php echo $row1["category_name"]; ?></td>
                                    <td><?php echo $row1["post"]; ?></td>
                                    <td class='edit'>
                                        <a href='update-category.php?categoryId=<?php echo $row1["category_id"] ?>'>
                                            <i class='fa fa-edit'></i>
                                        </a>
                                    </td>
                                    <td class='delete'>
                                        <a href='delete-category.php?categoryId=<?php echo $row1["category_id"] ?>'>
                                            <i class='fa fa-trash-o'></i>
                                        </a>
                                    </td>
                                    <!-- <td class='delete'><a href='delete-category.php'><i class='fa fa-trash-o'></i></a></td> -->
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
                <ul class='pagination admin-pagination'>
                    <li class="active"><a>1</a></li>
                    <li><a>2</a></li>
                    <li><a>3</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>