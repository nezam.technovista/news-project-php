<?php
include "header.php";
include "check-user.php";

if (isset($_POST['save'])) {
    include "config.php";
    $category_name = mysqli_real_escape_string($conn, $_POST["category_name"]);
    $post = mysqli_real_escape_string($conn, $_POST["post"]);
    $sql1 = "SELECT category_name FROM category WHERE category_name = '{$category_name}'";
    $query1 = mysqli_query($conn, $sql1) or die("Query Faild");
    if (mysqli_num_rows($query1) > 0) {
        echo "category name already exists";
    } else {
        $sql2 = "INSERT INTO category(category_name, post) VALUES('{$category_name}','{$post}') ";
        $query2 = mysqli_query($conn, $sql2);
        if (!$query2) {
            echo "can not add category";
        } else {
            header("Location: {$host_name}/admin/category.php");
            mysqli_close($conn);
        }
    }
}

?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="admin-heading">Add New Category</h1>
            </div>
            <div class="col-md-offset-3 col-md-6">
                <!-- Form Start -->
                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="category_name" class="form-control" placeholder="Category Name" required>
                        <input type="number" name="post" class="form-control" placeholder="Number of post" required>
                    </div>
                    <input type="submit" name="save" class="btn btn-primary" value="Save" required />
                </form>
                <!-- /Form End -->
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>