<?php
include "header.php";
include "check-user.php";

if (isset($_POST['submit'])) {
    include "config.php";
    $category_id = mysqli_real_escape_string($conn, $_POST["category_id"]);
    $category_name = mysqli_real_escape_string($conn, $_POST["category_name"]);
    // $post = mysqli_real_escape_string($conn, $_POST["post"]);

    $sql1 = " UPDATE category SET category_name = '{$category_name}' WHERE category_id = {$category_id} ";
    $query1 = mysqli_query($conn, $sql1) or die("sorry Query faild");

    if (!$query1) {
        echo "<p>can not update category.</p>";
    } else {
        header("Location: {$host_name}/admin/category.php");
        mysqli_close($conn);
    }
}

?>
<div id="admin-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="adin-heading"> Update Category</h1>
            </div>
            <div class="col-md-offset-3 col-md-6">
                <?php
                include "config.php";
                $category_id = $_GET["categoryId"];
                $sql2 = "SELECT * FROM category WHERE category_id = {$category_id} ";
                $query2 = mysqli_query($conn, $sql2) or die("Query Faild");
                if (mysqli_num_rows($query2) > 0) {
                    while ($row1 = mysqli_fetch_assoc($query2)) {


                ?>
                        <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="category_id" class="form-control" value="<?php echo $row1['category_id'] ?>" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Category Name</label>
                                <input type="text" name="category_name" class="form-control" value="<?php echo $row1['category_name'] ?>" placeholder="" required>
                            </div>
                            <input type="submit" name="sumbit" class="btn btn-primary" value="Update" required />
                        </form>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>