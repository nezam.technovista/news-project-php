<?php include 'header.php'; ?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- post-container -->
                <div class="post-container">
                    <?
                    include "config.php";

                    if (isset($_GET["cid"])) {
                        $category_id = $_GET["cid"];
                    }
                    $sql2 = "SELECT * FROM category WHERE category_id = {$category_id}";
                    $query2 = mysqli_query($conn, $sql2) or die("Query faild");
                    $row2 = mysqli_fetch_assoc($query2);
                    ?>
                    <h2 class="page-heading"><?php echo $row2["category_name"] ?></h2>
                    <?php

                    $limit = 3;
                    if (isset($_GET["pageId"])) {
                        $page = $_GET["pageId"];
                    } else {
                        $page = 1;
                    }
                    $offset = ($page - 1) * $limit;
                    $sql1 = "SELECT post.post_id, post.title, post.description, post.post_date, post.category, post.post_img, post.author, category.category_name, user.user_name FROM post 
                    LEFT JOIN category ON post.category = category.category_id
                    LEFT JOIN user ON post.author = user.user_id
                    WHERE post.category={$category_id}
                    ORDER BY post.post_id DESC LIMIT {$offset}, {$limit}";

                    $query1 = mysqli_query($conn, $sql1) or die("Query Faild");
                    if (mysqli_num_rows($query1) > 0) {
                        while ($row = mysqli_fetch_assoc($query1)) {
                    ?>

                            <div class="post-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a class="post-img" href="single.php?id=<?php echo $row["post_id"]; ?>"><img src="admin/upload/<?php echo $row["post_img"]; ?>" alt="" /></a>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="inner-content clearfix">
                                            <h3><a href='single.php?id=<?php echo $row["post_id"]; ?>'><?php echo $row["title"]; ?></a></h3>
                                            <div class="post-information">
                                                <span>
                                                    <i class="fa fa-tags" aria-hidden="true"></i>
                                                    <a href='category.php'><?php echo $row["category_name"]; ?></a>
                                                </span>
                                                <span>
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                    <a href='author.php?aid=<?php echo $row["author"]; ?>'><?php echo $row["user_name"]; ?></a>
                                                </span>
                                                <span>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    <?php echo $row["post_date"]; ?>
                                                </span>
                                            </div>
                                            <p class="description">
                                                <?php echo substr($row["description"], 0, 2) . "..."; ?>
                                            </p>
                                            <a class='read-more pull-right' href='single.php?id=<?php echo $row["post_id"]; ?>'>read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    } else {
                        echo "<h2>No record Found...</h2>";
                    }

                    if (mysqli_num_rows($query2) > 0) {
                        $total_records = $row2["post"];
                        $total_pages = ceil($total_records / $limit);
                        echo "<ul class='pagination admin-pagination'>";
                        if ($page > 1) {
                            echo "<li><a href='index.php?cid=" . $category_id . "&pageId=" . ($page - 1) . "'>Prev</a></li>";
                        }
                        for ($i = 1; $i <= $total_pages; $i++) {
                            if ($i == $page) {
                                $active = "active";
                            } else {
                                $active = "";
                            }
                            echo "<li class='" . $active . "'><a href='index.php?cid=" . $category_id . "&pageId=$i'>" . "$i" . "</a></li>";
                        }
                        if ($total_pages > $page) {
                            echo "<li><a href='index.php?cid=" . $category_id . "&pageId=" . ($page + 1) . "'>Next</a></li>";
                        }
                        echo "</ul>";
                    }

                    ?>
                </div><!-- /post-container -->
            </div>
            <?php include 'sidebar.php'; ?>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>